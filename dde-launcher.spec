%global sname dde-launcher

Name:           dde-launcher
Version:        5.5.37
Release:        2
Summary:        dde desktop-environment - Launcher module
License:        GPLv3
URL:            https://github.com/linuxdeepin/dde-launcher
Source0:        %{name}-%{version}.tar.gz
Source1:        org.deepin.dde.launcher.override.json

BuildRequires:  cmake
BuildRequires:  cmake(Qt5LinguistTools)
BuildRequires:  dtkwidget-devel
BuildRequires:  pkgconfig(dtkcore)
BuildRequires:  pkgconfig(dframeworkdbus)
BuildRequires:  pkgconfig(gsettings-qt)
BuildRequires:  pkgconfig(xcb-ewmh)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  pkgconfig(Qt5Svg)
BuildRequires:  pkgconfig(Qt5X11Extras)
BuildRequires:  qt5-qtbase-private-devel
BuildRequires:  gtest-devel gmock
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}
Requires:       deepin-menu
Requires:       dde-daemon
Requires:       startdde
Requires:       hicolor-icon-theme
Requires:       libqtxdg

%description
%{summary}.


%prep
%setup -q -n %{name}-%{version}
sed -i 's|lrelease|lrelease-qt5|' translate_generation.sh

%build
%cmake -DCMAKE_INSTALL_PREFIX=%{_prefix} -DWITHOUT_UNINSTALL_APP=1 .
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

# override
mkdir -p %{buildroot}/etc/dsg/configs/overrides/org.deepin.dde.launcher/
install -Dm644 %{SOURCE1} %{buildroot}/etc/dsg/configs/overrides/org.deepin.dde.launcher/

%files
%license LICENSE
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_bindir}/dde-launcher-wapper
%{_datadir}/applications/dde-launcher.desktop
%{_datadir}/dbus-1/services/*.service
%{_datadir}/icons/hicolor/scalable/apps/deepin-launcher.svg
%{_datadir}/glib-2.0/schemas/com.deepin.dde.launcher.gschema.xml
%{_datadir}/dsg/configs/org.deepin.dde.launcher/org.deepin.dde.launcher.json
%{_sysconfdir}/dsg/configs/overrides/org.deepin.dde.launcher/


%changelog
* Mon Sep 18 2023 leeffo <liweiganga@uniontech.com> - 5.5.37-2
- disable fullscreenmode

* Tue Aug 01 2023 leeffo <liweiganga@uniontech.com> - 5.5.37-1
- upgrade to version 5.5.37

* Thu Mar 30 2023 liweiganga <liweiganga@uniontech.com> - 5.4.45-1
- update: update to 5.4.45

* Mon Jul 18 2022 konglidong <konglidong@uniontech.com> - 5.4.13-1
- Update to 5.4.13

* Thu Jul 08 2021 weidong <weidong@uniontech.com> - 5.3.0.23-3
- update 5.3.0.23

* Thu Sep 3 2020 weidong <weidong@uniontech.com> - 5.1.0.6-3
- fix source url in spec

* Tue Aug 18 2020 chenbo pan <panchenbo@uniontech.com> - 5.1.0.6-2
- fix compile fail

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.1.0.6-1
- Package init
